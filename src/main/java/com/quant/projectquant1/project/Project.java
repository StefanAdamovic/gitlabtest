package com.quant.projectquant1.project;

import com.quant.projectquant1.client.Client;
import com.quant.projectquant1.client.DefaultClient;
import com.quant.projectquant1.user.DefaultUser;
import com.quant.projectquant1.user.User;

public interface Project {

    Client getClient();

    void setClient(DefaultClient client);

    User getProjectManager();

    void setProjectManager(DefaultUser projectManager);

    long getId();

    void setId(long id);

    String getName();

    void setName(String name);

}
