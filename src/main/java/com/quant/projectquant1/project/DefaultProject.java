package com.quant.projectquant1.project;

import com.quant.projectquant1.client.Client;
import com.quant.projectquant1.client.DefaultClient;
import com.quant.projectquant1.user.DefaultUser;
import com.quant.projectquant1.user.User;
import jakarta.persistence.*;


@Entity
@Table(name = "project")
public class DefaultProject implements Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name", unique = true)
    private String name;


    @OneToOne
    @JoinColumn(name = "project_manager_id")
    private DefaultUser projectManager;

    @OneToOne
    @JoinColumn(name = "client_id")
    private DefaultClient client;

    public DefaultProject(DefaultClient client, String name, DefaultUser projectManager) {

        this.client = client;
        this.name = name;
        this.projectManager = projectManager;
    }

    public DefaultProject(DefaultClient client, long id, String name, DefaultUser projectManager) {
        this.client = client;
        this.id = id;
        this.name = name;
        this.projectManager = projectManager;
    }

    @Override
    public String toString() {
        return "DefaultProject{" +
                "client=" + client +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", projectManager=" + projectManager +
                '}';
    }

    public DefaultProject() {
    }

    @Override
    public Client getClient() {
        return client;
    }


    @Override
    public void setClient(DefaultClient client) {
        this.client = client;
    }

    @Override
    public User getProjectManager() {
        return projectManager;
    }

    @Override
    public void setProjectManager(DefaultUser projectManager) {
        this.projectManager = projectManager;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
