package com.quant.projectquant1.project;

import com.quant.projectquant1.client.DefaultClient;
import com.quant.projectquant1.user.DefaultUser;
import com.quant.projectquant1.user.UserDao;
import com.quant.projectquant1.util.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class ProjectDao {


    private static final Logger LOGGER = LogManager.getLogger(UserDao.class);

    private static ProjectDao instance;
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    private List<DefaultProject> projectList;

    private DefaultProject project;

    private ProjectDao() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static ProjectDao getInstance() {
        if (instance == null) {
            instance = new ProjectDao();
        }
        return instance;
    }

    public void createProject(DefaultProject project) {
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(project);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    public List<DefaultProject> getAllProjects() {

        projectList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();



            Query<DefaultProject> query = session.createNativeQuery("select * from project", DefaultProject.class);
            projectList = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return projectList;
    }


    public DefaultProject getProjectById(int id) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultProject> query = session.createNativeQuery("select * from project where id = :projectId", DefaultProject.class);
            query.setParameter("projectId", id);
            project = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return project;
    }

    public List<DefaultProject> getProjectsByClient(DefaultClient client) {

        projectList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultProject> query = session.createNativeQuery("select * from project where client_id = :client_id", DefaultProject.class);
            query.setParameter("client_id", client.getId());
            projectList = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return projectList;
    }
}
