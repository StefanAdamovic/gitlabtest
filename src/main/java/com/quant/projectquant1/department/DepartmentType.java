package com.quant.projectquant1.department;

public enum DepartmentType {
    SALE, DESIGN, MANUFACTURE, ASSEMBLY
}
