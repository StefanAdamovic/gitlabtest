package com.quant.projectquant1.department;

import com.quant.projectquant1.util.HibernateUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;


public class DepartmentDao {

    private static final Logger LOGGER = LogManager.getLogger(DepartmentDao.class);

    private static DepartmentDao instance;
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    private List<DefaultDepartment> departmentList;

    private DefaultDepartment department;

    private DepartmentDao() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static DepartmentDao getInstance() {
        if (instance == null) {
            instance = new DepartmentDao();
        }
        return instance;
    }

    public void createDepartment(DefaultDepartment department) {
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(department);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    public List<DefaultDepartment> getDepartmentList() {

        departmentList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultDepartment> query = session.createNativeQuery("select * from department", DefaultDepartment.class);
            departmentList = query.getResultList();
            // System.out.println("Department list in Department DAO get list");
            // System.out.println(departmentList);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return departmentList;
    }


    public DefaultDepartment getDepartmentById(int id) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultDepartment> query = session.createNativeQuery("select * from department where id = :departmentId", DefaultDepartment.class);
            query.setParameter("departmentId", id);
            department = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return department;
    }

    public DefaultDepartment getDepartmentByName(DepartmentType departmentType) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultDepartment> query = session.createNativeQuery("select * from department where name = :departmentType", DefaultDepartment.class);
            query.setParameter("departmentType", departmentType.toString());
            department = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return department;
    }
}
