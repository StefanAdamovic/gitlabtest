package com.quant.projectquant1.department;

import jakarta.persistence.*;

@Entity
@Table(name = "department")
public class DefaultDepartment implements Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "name", unique = true)
    private String name;

    public DefaultDepartment(String name) {
        this.name = name;
    }

    public DefaultDepartment(DepartmentType type) {
        this.name = type.toString();
    }

    public DefaultDepartment() {
    }

    public DefaultDepartment(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "DefaultDepartment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
