package com.quant.projectquant1.department;

public interface Department {

    long getId();

    void setId(long id);

    String getName();

    void setName(String name);
}
