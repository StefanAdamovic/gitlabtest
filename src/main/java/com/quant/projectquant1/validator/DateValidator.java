package com.quant.projectquant1.validator;

import java.time.LocalDate;

public class DateValidator {

    private static DateValidator instance;
    public static DateValidator getInstance() {
        if (instance == null) {
            instance = new DateValidator();
        }
        return instance;
    }

    private DateValidator(){
    }

    private boolean dateIsNull(LocalDate date) {
        return date == null;
    }

    private boolean dateStringIsNull(String date) {

        if(date == null) return true;
        if (date.isEmpty() || date.isBlank())
            return true;
        //TODO format pattern check need to implement
        return false;
    }

    public boolean valid(LocalDate date){
        if (dateIsNull(date))
            return false;

        //TODO implement other methods for checking min and max allowed age in app
        return true;
    }

    public boolean valid(String date){
        if (dateStringIsNull(date))
            return false;

        //TODO implement other methods for checking min and max allowed age in app
        return true;
    }
}
