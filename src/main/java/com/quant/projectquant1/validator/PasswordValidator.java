package com.quant.projectquant1.validator;



public class PasswordValidator {

    private static PasswordValidator instance;

    public static PasswordValidator getInstance() {
        if (instance == null) {
            instance = new PasswordValidator();
        }
        return instance;
    }

    private PasswordValidator(){
    }

    private boolean nullOrEmpty(String password) {

        if (password == null)
            return true;

        if (password.isEmpty() || password.isBlank())
            return true;

        return false;
    }

    private boolean patternCheck(String password) {

        //TODO implement pattern check
        return true;
    }

    public boolean valid(String password){
        if (nullOrEmpty(password))
            return false;
        if (!patternCheck(password))
            return false;
        return true;
    }
}
