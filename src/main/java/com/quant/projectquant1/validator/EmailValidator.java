package com.quant.projectquant1.validator;

import java.util.List;

public class EmailValidator {

    private static EmailValidator instance;

    public static EmailValidator getInstance() {
        if (instance == null) {
            instance = new EmailValidator();
        }
        return instance;
    }

    private EmailValidator(){
    }

    private boolean nullOrEmpty(String email) {

        if (email == null)
            return true;

        if (email.isEmpty() || email.isBlank())
            return true;

        return false;
    }

    private boolean patternCheck(String email) {

        //TODO implement pattern check
        return true;
    }

    public boolean valid(String email){
        if (nullOrEmpty(email))
            return false;
        if (!patternCheck(email))
            return false;
        return true;
    }

}

