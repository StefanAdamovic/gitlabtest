package com.quant.projectquant1.status;

import com.quant.projectquant1.util.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class StatusDao {

    private static final Logger LOGGER = LogManager.getLogger(StatusDao.class);
    private static StatusDao instance;
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    private List<DefaultStatus> statusList;

    private DefaultStatus status;

    private StatusDao() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static StatusDao getInstance() {
        if (instance == null) {
            instance = new StatusDao();
        }
        return instance;
    }

    public void createStatus(DefaultStatus status) {
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(status);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    public List<DefaultStatus> getStatusList() {

        statusList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultStatus> query = session.createNativeQuery("select * from status", DefaultStatus.class);
            statusList = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return statusList;
    }


    public DefaultStatus getStatusById(int id) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultStatus> query = session.createNativeQuery("select * from status where id = :statusId", DefaultStatus.class);
            query.setParameter("statusId", id);
            status = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return status;
    }

    public DefaultStatus getStatusByName(StatusType statusType) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultStatus> query = session.createNativeQuery("select * from status where name = :statusType", DefaultStatus.class);
            query.setParameter("statusType", statusType.toString());
            status = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return status;
    }
}
