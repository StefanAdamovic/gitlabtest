package com.quant.projectquant1.status;

public interface Status {

    long getId();

    String getName();

    void setId(long id);

    void setName(String name);
}
