package com.quant.projectquant1.status;

public enum StatusType {

    IN_PROGRESS, COMPLETED, STOPPED, PAUSED, WAITING_REVIEW, ACCEPTED, REJECTED
}
