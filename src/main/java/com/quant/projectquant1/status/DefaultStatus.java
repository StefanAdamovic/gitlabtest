package com.quant.projectquant1.status;

import jakarta.persistence.*;

@Entity
@Table(name = "status")
public class DefaultStatus implements Status{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name", unique = true)
    private String name;

    public DefaultStatus(String name) {
        this.name = name;
    }

    public DefaultStatus(StatusType type) {
        this.name = type.toString();
    }

    public DefaultStatus(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public DefaultStatus() {
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
