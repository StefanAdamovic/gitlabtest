package com.quant.projectquant1.controller;

import com.quant.projectquant1.user.User;
import com.quant.projectquant1.user.UserService;
import com.quant.projectquant1.util.Page;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

@WebServlet(name = "sign-in", value = "/sign-in")
public class SignInController extends HttpServlet {


    private UserService userService;

    private static final Logger LOGGER = LogManager.getLogger(SignInController.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher(Page.SIGN_IN.getValue()).forward(request, response);
        LOGGER.info("Sign in controller - doGet notImplemented");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        userService = new UserService();
        User user;

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String errMsg = "";

        if (!userService.signInUser(email, password)) {
            errMsg = "Wrong email or password please try again";
            request.setAttribute("errMsg", errMsg);
            request.getRequestDispatcher(Page.SIGN_IN.getValue()).forward(request, response);
            LOGGER.info(errMsg);
            return;
        }

        user = userService.getUserByEmail(email);

        if (user == null) {
            errMsg = "Something went wrong please try again";
            request.setAttribute("errMsg", errMsg);
            request.getRequestDispatcher(Page.SIGN_IN.getValue()).forward(request, response);
            LOGGER.info(errMsg);
            return;
        }

        request.getSession().setAttribute("user", user);
        request.getRequestDispatcher(Page.HOME.getValue()).forward(request, response);
        LOGGER.info("Log in success: Email:" + user.getEmail() + " Password: " + user.getPassword());


    }
}
