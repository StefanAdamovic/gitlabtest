package com.quant.projectquant1.controller;

import com.quant.projectquant1.user.User;
import com.quant.projectquant1.user.UserService;
import com.quant.projectquant1.util.Page;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

@WebServlet(name = "sign-up", value = "/sign-up")
public class SignUpController extends HttpServlet {


    private UserService userService;

    private static final Logger LOGGER = LogManager.getLogger(SignUpController.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher(Page.SIGN_UP.getValue()).forward(request, response);
        LOGGER.info("Sign up controller - doGet notImplemented");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        userService = new UserService();

        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String department = request.getParameter("department");
        String userRole = request.getParameter("userRole");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String errMsg = "";
        String accountCreatedMsg = "";

        if (!userService.signUpUser(email, password, dateOfBirth, department, userRole, firstName, lastName)) {
            errMsg = "Error in creating account please try again";
            request.setAttribute("errMsg", errMsg);
            request.getRequestDispatcher(Page.SIGN_UP.getValue()).forward(request, response);
            LOGGER.info(errMsg);
            return;
        }

        accountCreatedMsg = "Thank you for creation of account. Please Sign in";
        request.setAttribute("accountCreatedMsg", accountCreatedMsg);
        request.getRequestDispatcher(Page.SIGN_IN.getValue()).forward(request, response);
        LOGGER.info("Account creation: success - Email: "+user.getEmail() + " / Password:" + user.getPassword());


    }
}
