package com.quant.projectquant1.util;

import com.quant.projectquant1.client.DefaultClient;
import com.quant.projectquant1.department.DefaultDepartment;

import com.quant.projectquant1.project.DefaultProject;
import com.quant.projectquant1.status.DefaultStatus;
import com.quant.projectquant1.user.DefaultUser;
import com.quant.projectquant1.userRole.DefaultUserRole;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.addAnnotatedClass(DefaultDepartment.class); // Add your entity class here
            configuration.addAnnotatedClass(DefaultStatus.class); // Add your entity class here
            configuration.addAnnotatedClass(DefaultUserRole.class); // Add your entity class here
            configuration.addAnnotatedClass(DefaultUser.class); // Add your entity class here
            configuration.addAnnotatedClass(DefaultClient.class); // Add your entity class here
            configuration.addAnnotatedClass(DefaultProject.class); // Add your entity class here
            configuration.configure(); // Load Hibernate configuration from hibernate.cfg.xml or similar
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Session Factory could not be created." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }


    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
