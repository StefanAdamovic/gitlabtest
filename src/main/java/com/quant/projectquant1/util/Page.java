package com.quant.projectquant1.util;

public enum Page {

    SIGN_IN("sign-in.jsp"), SIGN_UP("sign-up.jsp"), HOME("home.jsp");

    private String value;

    private Page(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
