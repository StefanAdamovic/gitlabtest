package com.quant.projectquant1;

import com.quant.projectquant1.client.ClientDao;
import com.quant.projectquant1.client.DefaultClient;
import com.quant.projectquant1.department.DepartmentType;
import com.quant.projectquant1.department.DefaultDepartment;
import com.quant.projectquant1.department.DepartmentDao;
import com.quant.projectquant1.project.DefaultProject;
import com.quant.projectquant1.project.ProjectDao;
import com.quant.projectquant1.status.StatusType;
import com.quant.projectquant1.status.DefaultStatus;
import com.quant.projectquant1.status.StatusDao;
import com.quant.projectquant1.user.DefaultUser;
import com.quant.projectquant1.user.UserDao;
import com.quant.projectquant1.userRole.UserRoleType;
import com.quant.projectquant1.userRole.DefaultUserRole;
import com.quant.projectquant1.userRole.UserRoleDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;


public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);


    public static void main(String[] args) {

        LOGGER.info("Test info logera");
        LOGGER.error("Test error logera");
        LOGGER.debug("Test debug logera");

        System.out.println("Starting program");

        DefaultDepartment department = new DefaultDepartment();
        DefaultDepartment department2 = new DefaultDepartment();
        DefaultDepartment department3 = new DefaultDepartment();
        DefaultDepartment department4 = new DefaultDepartment();

        department.setName(DepartmentType.SALE.toString());
        department2.setName(DepartmentType.DESIGN.toString());
        department3.setName(DepartmentType.MANUFACTURE.toString());
        department4.setName(DepartmentType.ASSEMBLY.toString());

        DepartmentDao departmentDao = DepartmentDao.getInstance();

        departmentDao.createDepartment(department);
        departmentDao.createDepartment(department2);
        departmentDao.createDepartment(department3);
        departmentDao.createDepartment(department4);

        System.out.println();
        System.out.println("Departments by list in program");
        System.out.println(departmentDao.getDepartmentList());

        System.out.println();
        System.out.println("Departments by id in program");
        System.out.println(departmentDao.getDepartmentById(1));
        System.out.println(departmentDao.getDepartmentByName(DepartmentType.DESIGN));


        ArrayList<DefaultStatus> statuses =
                new ArrayList<>(
                        Arrays.asList(
                                new DefaultStatus(StatusType.COMPLETED),
                                new DefaultStatus(StatusType.PAUSED),
                                new DefaultStatus(StatusType.IN_PROGRESS),
                                new DefaultStatus(StatusType.WAITING_REVIEW),
                                new DefaultStatus(StatusType.STOPPED),
                                new DefaultStatus(StatusType.ACCEPTED),
                                new DefaultStatus(StatusType.REJECTED)
                        )
                );

        StatusDao statusDao = StatusDao.getInstance();

        for (DefaultStatus ds : statuses) {
            statusDao.createStatus(ds);
        }

        System.out.println();
        System.out.println("Status by list in program");
        System.out.println(statusDao.getStatusList());

        System.out.println();
        System.out.println("Departments by id in program");
        System.out.println(statusDao.getStatusById(1));
        System.out.println(statusDao.getStatusByName(StatusType.PAUSED));


        ArrayList<DefaultUserRole> userRoles =
                new ArrayList<>(
                        Arrays.asList(
                                new DefaultUserRole(UserRoleType.ADMIN),
                                new DefaultUserRole(UserRoleType.ARCHITECT),
                                new DefaultUserRole(UserRoleType.PROJECT_MANAGER),
                                new DefaultUserRole(UserRoleType.CIVIL_ENGINEER)
                        )
                );

        UserRoleDao userRoleDao = UserRoleDao.getInstance();

        for (DefaultUserRole dur : userRoles) {
            userRoleDao.createUserRole(dur);
        }

        System.out.println();
        System.out.println("User roles by list in program");
        System.out.println(userRoleDao.getUserRoleList());

        System.out.println();
        System.out.println("User Role by id in program");
        System.out.println(userRoleDao.getUserRoleById(1));
        System.out.println(userRoleDao.getUserRoleByName(UserRoleType.ARCHITECT));


        ArrayList<DefaultUser> users =
                new ArrayList<>(
                        Arrays.asList(
                                new DefaultUser(
                                        "stef.adamovic@gmail.com", "pass123", "Stefan",
                                        "Adamovic", LocalDate.of(1998, 9, 21),
                                        departmentDao.getDepartmentByName(DepartmentType.DESIGN),
                                        userRoleDao.getUserRoleByName(UserRoleType.ARCHITECT)),
                                new DefaultUser(
                                        "bojan.lekic@gmail.com", "pass123", "Bojan",
                                        "Lekic", LocalDate.of(1980, 12, 9),
                                        departmentDao.getDepartmentByName(DepartmentType.DESIGN),
                                        userRoleDao.getUserRoleByName(UserRoleType.CIVIL_ENGINEER)),
                                new DefaultUser(
                                        "hani.kadvanj@gmail.com", "pass123", "Hani",
                                        "Kadvanj", LocalDate.of(1984, 6, 25),
                                        departmentDao.getDepartmentByName(DepartmentType.DESIGN),
                                        userRoleDao.getUserRoleByName(UserRoleType.PROJECT_MANAGER)),
                                new DefaultUser(
                                        "robert.test@gmail.com", "pass123", "Robert",
                                        "TestLastName", LocalDate.of(1975, 1, 1),
                                        departmentDao.getDepartmentByName(DepartmentType.MANUFACTURE),
                                        userRoleDao.getUserRoleByName(UserRoleType.PROJECT_MANAGER)),
                                new DefaultUser(
                                        "alen.test@gmail.com", "pass123", "Alen",
                                        "TestLastName", LocalDate.of(1977, 1, 1),
                                        departmentDao.getDepartmentByName(DepartmentType.ASSEMBLY),
                                        userRoleDao.getUserRoleByName(UserRoleType.PROJECT_MANAGER)),
                                new DefaultUser(
                                        "jelena.jovanovic@gmail.com", "pass123", "Jelena",
                                        "Jovanovic", LocalDate.of(1986, 7, 7),
                                        departmentDao.getDepartmentByName(DepartmentType.SALE),
                                        userRoleDao.getUserRoleByName(UserRoleType.PROJECT_MANAGER)),
                                new DefaultUser(
                                        "jelena.jovanovic@gmail.com", "pass123", "Jelena",
                                        "Jovanovic", LocalDate.of(1986, 7, 7),
                                        departmentDao.getDepartmentByName(DepartmentType.ASSEMBLY),
                                        userRoleDao.getUserRoleByName(UserRoleType.PROJECT_MANAGER))


                        )
                );

        UserDao userDao = UserDao.getInstance();

        for (DefaultUser du : users) {
            userDao.createUser(du);
        }

        System.out.println();
        System.out.println("All users in program");
        System.out.println(userDao.getAllUsers());

        System.out.println();
        System.out.println("User by id in program");
        System.out.println(userDao.getUserById(1));

        System.out.println();
        System.out.println("User by email in program");
        System.out.println(userDao.getUserByEmail("alen.test@gmail.com"));

        ArrayList<DefaultClient> clients =
                new ArrayList<>(
                        Arrays.asList(
                                new DefaultClient("CTP", "Serbia"),
                                new DefaultClient("BEKAMENT", "Serbia"),
                                new DefaultClient("MARTINI", "Italy"),
                                new DefaultClient("REISSWOLF", "GERMANY")
                        )
                );

        ClientDao clientDao = ClientDao.getInstance();

        for (DefaultClient dc : clients) {
            clientDao.createClient(dc);
        }

        System.out.println();
        System.out.println("Clients by list in program");
        System.out.println(clientDao.getAllClients());

        System.out.println();
        System.out.println("Client by id in program");
        System.out.println(clientDao.getClientById(1));
        System.out.println(clientDao.getClientByNamel("MARTINI"));


        ArrayList<DefaultProject> projects =
                new ArrayList<>(
                        Arrays.asList(
                                new DefaultProject(clientDao.getClientById(4), "TestProject1", userDao.getUserById(1)),
                                new DefaultProject(clientDao.getClientById(1), "TestProject2", userDao.getUserById(1)),
                                new DefaultProject(clientDao.getClientById(2), "TestProject3", userDao.getUserById(4)),
                                new DefaultProject(clientDao.getClientById(1), "TestProject4", userDao.getUserById(4))
                        )
                );

        ProjectDao projectDao = ProjectDao.getInstance();

        for (DefaultProject dp : projects) {
            projectDao.createProject(dp);
        }

        System.out.println();
        System.out.println("Projects by list in program");
        System.out.println(projectDao.getAllProjects());

        System.out.println();
        System.out.println("Project by id in program");
        System.out.println(projectDao.getProjectById(1));
        System.out.println(projectDao.getProjectsByClient(clientDao.getClientById(2)));


    }
}
