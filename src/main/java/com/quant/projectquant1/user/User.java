package com.quant.projectquant1.user;

import com.quant.projectquant1.client.Client;
import com.quant.projectquant1.client.DefaultClient;
import com.quant.projectquant1.department.DefaultDepartment;
import com.quant.projectquant1.department.Department;
import com.quant.projectquant1.userRole.DefaultUserRole;
import com.quant.projectquant1.userRole.UserRole;


import java.time.LocalDate;

public interface User {

    long getId();

    void setId(long id);

    String getEmail();

    void setEmail(String email);

    String getPassword();

    void setPassword(String password);

    String  getFirstName();

    void setFirstName(String firstName);

    String getLastName();

    void setLastName(String lastName);

    LocalDate getDateOfBirth();

    void setDateOfBirth(LocalDate dateOfBirth);

    Department getDepartment();

    void setDepartment(DefaultDepartment department);

    UserRole getUserRole();

    void setUserRole(DefaultUserRole userRoleId);






}
