package com.quant.projectquant1.user;

import com.quant.projectquant1.util.HibernateUtil;
import jakarta.transaction.Transactional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class UserDao {

    private static final Logger LOGGER = LogManager.getLogger(UserDao.class);

    private static UserDao instance;
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    private List<DefaultUser> userList;

    private DefaultUser user;

    private UserDao() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static UserDao getInstance() {
        if (instance == null) {
            instance = new UserDao();
        }
        return instance;
    }

    public void createUser(DefaultUser user) {
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(user);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    public List<DefaultUser> getAllUsers() {

        userList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();



            Query<DefaultUser> query = session.createNativeQuery("select * from user", DefaultUser.class);
            userList = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return userList;
    }


    public DefaultUser getUserById(int id) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultUser> query = session.createNativeQuery("select * from user where id = :userId", DefaultUser.class);
            query.setParameter("userId", id);
            user = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return user;
    }

    public DefaultUser getUserByEmail(String email) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultUser> query = session.createNativeQuery("select * from user where email = :email", DefaultUser.class);
            query.setParameter("email", email);
            user = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return user;
    }


}
