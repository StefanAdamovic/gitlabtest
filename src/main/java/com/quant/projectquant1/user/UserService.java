package com.quant.projectquant1.user;

import com.quant.projectquant1.validator.DateValidator;
import com.quant.projectquant1.validator.EmailValidator;
import com.quant.projectquant1.validator.PasswordValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.List;

public class UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserService.class);
    private UserDao userDao;
    private EmailValidator emailValidator;
    private PasswordValidator passwordValidator;
    private DateValidator dateValidator;

    private DepartmentSerivce departmentSerivce;
    private UserRoleService userRoleService;

    private static boolean userIsNull(User user) {
        if (user == null) {
            LOGGER.info("User service: User is null");
            return true;
        }
        return false;
    }

    public boolean signInUser(String email, String password) {

        DefaultUser user;

        if (!emailValid(password)) return false;

        if (!passwordValid(password)) return false;

        user = (DefaultUser) getUserByEmail(email);

        if (userIsNull(user)) return false;

        return passwordMatch(user, password);
    }

    private boolean passwordMatch(User user, String password) {
        if (!user.getPassword().equals(password)) {
            LOGGER.info("User service: Password is not correct");
            return false;
        }

        return true;
    }

    private boolean emailValid(String email) {

        emailValidator = EmailValidator.getInstance();
        if (!emailValidator.valid(email)) {
            LOGGER.info("User service: Email not valid");
            return false;
        }
        return true;
    }

    private boolean passwordValid(String password) {
        passwordValidator = PasswordValidator.getInstance();
        if (!passwordValidator.valid(password)) {
            LOGGER.info("User service: Password not valid");
            return false;
        }
        return true;
    }

    private boolean dateOfBirthValid(LocalDate date) {
        dateValidator = DateValidator.getInstance();
        if (!dateValidator.valid(date)) {
            LOGGER.info("User service: Date of birth LocalDate not valid");
            return false;
        }
        return true;
    }

    private boolean dateOfBirthValid(String date) {
        dateValidator = DateValidator.getInstance();
        if (!dateValidator.valid(date)) {
            LOGGER.info("User service: Date of birth String not valid");
            return false;
        }
        return true;
    }

    public User getUserByEmail(String email) {
        userDao = UserDao.getInstance();
        return userDao.getUserByEmail(email);
    }

    public List<DefaultUser> getAllUsers() {
        userDao = UserDao.getInstance();
        return userDao.getAllUsers();
    }

    public boolean signUpUser(String email, String password, String dateOfBirth, String department, String userRole, String firstName, String lastName) {

        DefaultUser user;
        LocalDate dob;

        if (!emailValid(password)) return false;
        if (!passwordValid(password)) return false;
        if (!dateOfBirthValid(dateOfBirth)) return false;
        dob = LocalDate.parse(dateOfBirth);
        if (!departmentValid(department)) return false;
        if (!userRoleValid(userRole)) return false;
        if (!nameValid(firstName)) return false;
        if (!nameValid(lastName)) return false;

        user = new DefaultUser(email, password, dateOfBirth, department, userRole, firstName, lastName);

        createUser(user);

        return true;

    }

    private void createUser(DefaultUser user) {
        userDao = UserDao.getInstance();
        userDao.createUser(user);
    }
}
