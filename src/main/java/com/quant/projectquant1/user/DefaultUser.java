package com.quant.projectquant1.user;


import com.quant.projectquant1.department.DefaultDepartment;
import com.quant.projectquant1.department.Department;
import com.quant.projectquant1.userRole.DefaultUserRole;
import com.quant.projectquant1.userRole.UserRole;
import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
@Table(name = "user")
public class DefaultUser implements User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Override
    public String toString() {
        return "DefaultUser{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", department=" + department +
                ", userRole=" + userRole +
                '}';
    }

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @OneToOne
    @JoinColumn(name = "department_id")
    private DefaultDepartment department;

    @OneToOne
    @JoinColumn(name = "user_role_id")
    private DefaultUserRole userRole;


    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public Department getDepartment() {
        return department;
    }

    @Override
    public void setDepartment(DefaultDepartment department) {
        this.department = department;
    }

    @Override
    public UserRole getUserRole() {
        return userRole;
    }

    @Override
    public void setUserRole(DefaultUserRole userRole) {
        this.userRole = userRole;
    }

    public DefaultUser(String email, String password, String firstName, String lastName, LocalDate dateOfBirth, DefaultDepartment department, DefaultUserRole userRole) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.department = department;
        this.userRole = userRole;
    }

    public DefaultUser(long id, String email, String password, String firstName, String lastName, LocalDate dateOfBirth, DefaultDepartment department, DefaultUserRole userRole) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.department = department;
        this.userRole = userRole;
    }

    public DefaultUser() {
    }
}
