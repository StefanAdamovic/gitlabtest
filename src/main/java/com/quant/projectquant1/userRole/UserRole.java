package com.quant.projectquant1.userRole;

public interface UserRole {

    long getId();

    void setId(long id);

    String getName();

    void setName(String name);
}
