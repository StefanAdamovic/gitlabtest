package com.quant.projectquant1.userRole;

import com.quant.projectquant1.department.DepartmentDao;
import com.quant.projectquant1.util.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class UserRoleDao {

    private static final Logger LOGGER = LogManager.getLogger(DepartmentDao.class);

    private static UserRoleDao instance;
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    private List<DefaultUserRole> userRoleList;

    private DefaultUserRole userRole;

    private UserRoleDao() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static UserRoleDao getInstance() {
        if (instance == null) {
            instance = new UserRoleDao();
        }
        return instance;
    }

    public void createUserRole(DefaultUserRole userRole) {
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(userRole);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    public List<DefaultUserRole> getUserRoleList() {

        userRoleList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultUserRole> query = session.createNativeQuery("select * from user_role", DefaultUserRole.class);
            userRoleList = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return userRoleList;
    }


    public DefaultUserRole getUserRoleById(int id) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultUserRole> query = session.createNativeQuery("select * from user_role where id = :userRoleId", DefaultUserRole.class);
            query.setParameter("userRoleId", id);
            userRole = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return userRole;
    }

    public DefaultUserRole getUserRoleByName(UserRoleType userRoleType) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultUserRole> query = session.createNativeQuery("select * from user_role where name = :userRoleType", DefaultUserRole.class);
            query.setParameter("userRoleType", userRoleType.toString());
            userRole = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return userRole;
    }
}
