package com.quant.projectquant1.userRole;

import jakarta.persistence.*;

@Entity
@Table(name = "user_role")
public class DefaultUserRole implements UserRole {
    public DefaultUserRole(String name) {
        this.name = name;
    }

    public DefaultUserRole(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public DefaultUserRole() {
    }

    public DefaultUserRole(UserRoleType type) {
        this.name = type.toString();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "name", unique = true)
    private String name;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DefaultUserRole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
