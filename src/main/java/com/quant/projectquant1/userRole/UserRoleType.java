package com.quant.projectquant1.userRole;

public enum UserRoleType {

    ADMIN, PROJECT_MANAGER, CIVIL_ENGINEER, ARCHITECT
}
