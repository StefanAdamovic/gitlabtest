package com.quant.projectquant1.client;

import com.quant.projectquant1.user.DefaultUser;
import com.quant.projectquant1.user.UserDao;
import com.quant.projectquant1.util.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class ClientDao {

    private static final Logger LOGGER = LogManager.getLogger(ClientDao.class);

    private static ClientDao instance;
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    private List<DefaultClient> clientList;

    private DefaultClient client;

    private ClientDao() {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    public static ClientDao getInstance() {
        if (instance == null) {
            instance = new ClientDao();
        }
        return instance;
    }

    public void createClient(DefaultClient client) {
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(client);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    public List<DefaultClient> getAllClients() {

        clientList = new ArrayList<>();

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            Query<DefaultClient> query = session.createNativeQuery("select * from client", DefaultClient.class);
            clientList = query.getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return clientList;
    }


    public DefaultClient getClientById(int id) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultClient> query = session.createNativeQuery("select * from client where id = :clientId", DefaultClient.class);
            query.setParameter("clientId", id);
            client = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return client;
    }

    public DefaultClient getClientByNamel(String name) {

        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();

            Query<DefaultClient> query = session.createNativeQuery("select * from client where name = :name", DefaultClient.class);
            query.setParameter("name", name);
            client = query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error(e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return client;
    }
}
