package com.quant.projectquant1.client;

public interface Client {
    long getId();

    void setId(long id);

    String getName();

    void setName(String name);

    String getCountry();

    void setCountry(String country);


}
