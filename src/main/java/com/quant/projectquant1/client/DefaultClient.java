package com.quant.projectquant1.client;

import jakarta.persistence.*;

@Entity
@Table(name = "client")
public class DefaultClient implements Client{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "country")
    private String country;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    @Override
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public DefaultClient() {
    }

    public DefaultClient(long id, String name, String country) {
        this.id = id;
        this.name = name.toUpperCase();
        this.country = country;
    }

    @Override
    public String toString() {
        return "DefaultClient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public DefaultClient(String name, String country) {
        this.name = name.toUpperCase();
        this.country = country;
    }
}
