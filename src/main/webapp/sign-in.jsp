<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign in</title>
</head>
<body>


<div class="container">
    <h2>Sign In</h2>
    <form action="sign-in" method="post">
        <input type="text" placeholder="Enter your email" name="email">
        <p>${errMsgEmail }</p>
        <input type="password"
               placeholder="Password" name="password">
        <p>${errMsgPassword }</p>
        <input type="submit" value="Sign in">
    </form>
</div>
</body>
</html>
