<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<c:if test="${sessionScope.user == null}">
    <%
        session.invalidate();
        response.sendRedirect("sign-in.jsp");
    %>
</c:if>
<head>
    <title>Home page</title>
</head>
<body>


<h2>Home page</h2>
<div class="container">
    <c:if test="${sessionScope.user != null}">
        <p>Name: ${sessionScope.user.getFirstName()} ${sessionScope.user.getLastName()}</p>
        <p>Email: ${sessionScope.user.email}</p>
        <p>Date of birth: ${sessionScope.user.getDateOfBirth()}</p>
        <p>Department: ${sessionScope.user.getDepartment()}</p>
        <p>User Role: ${sessionScope.user.getUserRole()}</p>
    </c:if>


</div>
</body>
</html>
